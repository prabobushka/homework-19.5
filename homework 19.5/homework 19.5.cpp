#include <iostream>
#include <array>
using namespace std;
struct Animal {
	virtual void voice()const = 0;
	virtual ~Animal() {}
};
struct Dog : virtual public Animal {
	void voice()const override {
		puts("Woof!");
	}
};
struct Cat : virtual public Animal {
	void voice()const override {
		puts("Meow!");
	}
};
struct Cow : virtual public Animal {
	void voice()const override {
		puts("Moo!");
	}
};
int main() {
	Dog a, b;
	Cat c, d;
	Cow e, f;
	array<Animal*, 6> animals{ &a, &e, &b, &c, &f, &d };
	for (const auto& pa : animals) pa->voice();
	system("pause > nul");
}
